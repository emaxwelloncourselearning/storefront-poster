require('dotenv').load();
const isDev = process.env.NODE_ENV === 'development';
const invokeMap = require('lodash/invokeMap');
const Promise = require('bluebird');
const cron = require('node-cron');
const database = require('./database');
const fetch = require('node-fetch');
const url = process.env.MAGENTO_URL;

if (isDev) {
    Promise.config({
        warnings: isDev,
        longStackTraces: isDev,
        monitoring: isDev
    });
}

database.connect();
const StoreFront = require('./models/StoreFront');
let mapper = function(store){
    let storeRequest = {};

    storeRequest.id = "";
    storeRequest.subdomain = "";
    storeRequest.creationDate = "utcdate";
    storeRequest.lastUpdatedDate = "utcdate";
    storeRequest.status = store.status;
    storeRequest.companyName = store.companyName;
    storeRequest.primaryContactName = store.primaryContactName;
    storeRequest.primaryContactEmail = store.primaryContactEmail;
    storeRequest.adminId = store.adminUserId;
    storeRequest.type = store.type;
    storeRequest.companyUrl = "";
    storeRequest.companyDetails = "";
    storeRequest.colors = {};
    if(store.colors){
        storeRequest.colors.primary = store.colors.brandPrimary;
        storeRequest.colors.secondary = store.colors.brandSecondary;
    }
    storeRequest.logoUrl = store.logoUrl;
    storeRequest.courseIds = []

    return storeRequest;

};

let postStore = function(store){
    fetch(url, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(mapper(store))
    })
    .then(function(res) {
        return res.json();
    }).then(function(json) {
        console.log(json);
    });
};

// Sync stores with publish flag = to true.
// Changes to model
    // Add flag called publish
       // If publish then post, update flag to false, if fails don't change, but update status to 'invalid' or 'error'
    // Add last published date
let task = function() {
    StoreFront.find().populate('Course')
        .execAsync()
        .then(docs => invokeMap(docs, 'toObject'))
        .then(objs => objs.map(obj => StoreFront.desaturate(obj)))
        .then(storefronts => storefronts.map(storefront => postStore(storefront)));

        //TODO: Add queue for failed stores
        //.catch(error());
};

let error = function(message){
    console.log(message);
};

task();

//cron.schedule('* * * * *', function() {
//    task();
//    console.log('running a task every minute');
//});