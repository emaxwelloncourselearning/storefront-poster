const mongoose = require('mongoose');
const Promise = require('bluebird');
const uri = process.env.MONGO_URI;
mongoose.Promise = Promise;

/*
 * from: http://blog.mongolab.com/2014/04/mongodb-driver-mongoose/
 * Mongoose by default sets the auto_reconnect option to true.
 * We recommend setting socket options at both the server and replica set level.
 * We recommend a 30 second connection timeout because it allows for
 * plenty of time in most operating environments.
 */
const options = {
    server: {
        socketOptions: {
            keepAlive: 1,
            connectTimeoutMS: 30000
        }
    }
};

// setup debugging
mongoose.connection.on('error', console.error.bind(console));
// promisify mongoose...cause it makes it so much easier to use
Promise.promisifyAll(mongoose);

// connect to our database
module.exports = {
    connect() {
        return new Promise(function(resolve, reject) {
            mongoose.connect(uri, options, err => {
                if (err) { return reject(err); }
                resolve();
            });
        });
    }
};