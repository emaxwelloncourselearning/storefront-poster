const mongoose = require('mongoose');
const schema = mongoose.Schema;

module.exports = mongoose.model('Course', schema({
	// sku
	_id: { type: String, required: true },
	description: String,
	title: { type: String, required: true },
	// vertical
	endMarket: { type: String, required: true },
	// filter
	states: [String],
	price: { type: Number, required: true },
	per: { type: String, required: true }
}));