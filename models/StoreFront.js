const mongoose = require('mongoose');
const moment = require('moment');
const pick = require('lodash/pick');
const { ObjectId } = mongoose.Types;

const schema = mongoose.Schema({
	_id: { type: String, required: true, default: () => new ObjectId() },
	creationDate: { type: Date, required: true, default: moment.utc },
	lastUpdatedDate: { type: Date, required: true, default: moment.utc },
	name: { type: String, required: true },
	type: { type: String, required: true },
	status: String,
	companyName: String,
	companyWebsite: String,
	adminUserId: String,
	primaryContactName: String,
	primaryContactEmail: String,
	companyDetails: { type: String, maxlength: 2000 },
	colors: {
		brandPrimary: { type: String },
		brandSecondary: { type: String }
	},
	logoUrl: String,
	courses: [{type: String, ref: 'Course'}],
	resellerAddress: {
		address1: { type: String, maxlength: 60 },
		address2: { type: String, maxlength: 60 },
		city: { type: String, maxlength: 60 },
		state: { type: String, maxlength: 4 },
		zip: { type: String, maxlength: 10 }
	}
});

schema.pre('findOneAndUpdate', function(next) {
	const update = this.getUpdate();
	update.lastUpdatedDate = moment.utc().format();
	next();
});

const model = module.exports = mongoose.model('StoreFront', schema);

Object.assign(model, {
	desaturate(obj) {
		return pick(obj, [
			'_id',
			'lastUpdatedDate',
			'status',
			'companyName',
			'adminUserId',
			'primaryContactEmail'
		]);
	}
});
