const mongoose = require('mongoose');
const schema = mongoose.Schema;

module.exports = mongoose.model('User', schema({
	_id: { type: String, required: true },
	name: { type: String, required: true }
}));